package com.example.tektonserverapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TektonServerappApplication {

	public static void main(String[] args) {
		SpringApplication.run(TektonServerappApplication.class, args);
	}

}
